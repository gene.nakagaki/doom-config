;;; ~/.doom.d/+function.el -*- lexical-binding: t; -*-

(defun gene/test ()
  "testing"
  (interactive)
  (org-copy-special)
  (->> (thing-at-point 'line)
      (message)))

(map!
 (:leader
  :desc "Gene Test" :n "j" #'gene/test))
