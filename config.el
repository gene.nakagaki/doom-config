;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "John Doe"
      user-mail-address "john@doe.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; (setq doom-font (font-spec :family "monospace" :size 12 :weight 'semi-light)
;;       doom-variable-pitch-font (font-spec :family "sans" :size 13))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-one)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/gtd/")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)


;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c g k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c g d') to jump to their definition and see how
;; they are implemented.

(after! org
  (require 'ox-publish)
  (add-to-list 'org-modules "ox-publish")
  (setq org-todo-keywords
        '((sequence "TODO(t)" "STARTED(s)" "PENDING(p)" "|" "DONE(D)" "DELEGATED(d)" "CANCELLED(c)")
          ))
  (setq org-todo-keyword-faces
        '(("STARTED" . (:foreground "Tomato" :weight bold))
          ("TODO" .(:foreground "Yellow" :weight bold))
          ("PENDING" . +org-todo-onhold)
          ("DELEGATED" +org-todo-onhold)
          ("DONE" .(:foreground "Chartreuse"))))
  (setq org-agenda-files
        '("~/gtd/inbox.org"
          "~/gtd/todo.org"
          "~/gtd/tickler.org"))
  (setq org-capture-templates
        '(("t" "Todo [inbox]" entry (file "~/gtd/inbox.org") "* TODO %i%? [%]")
          ("p" "Pull Request [inbox]" entry (file "~/gtd/inbox.org") "* TODO %i%? [%]
:PROPERTIES:
:GITLAB:
:PULL_REQUEST:
:END:
** TODO coding: [%]
- [ ] add unit test
- [ ] add [[https://drive.google.com/drive/folders/1kY_FXPi03xR25ifz3tifoNUPmbWQx39q][integration test]]
** TODO review: [%]
- [ ] f-kawamura
- [ ] n-machida
** TODO release: [%]")
          ("T" "Tickler" entry (file "~/gtd/tickler.org") "* %i%? \n %U")))
  (setq org-refile-targets
        '(("~/gtd/todo.org" :maxlevel . 2)
          ("~/gtd/someday.org" :level . 1)
          ("~/gtd/tickler.org" :maxlevel . 2)
          ("~/gtd/reference.org" :maxlevel . 2)))
  (setq org-publish-project-alist
        '(
          ("gene-org"
           :base-directory "~/gene-nakagaki/src/"
           :base-extension "org"
           :publishing-directory "~/gene-nakagaki/dist/"
           :recursive t
           :publishing-function org-html-publish-to-html
           :headline-levels 4             ; Just the default for this project.
           :auto-preamble t)
          ("gene-static"
           :base-directory "~/gene-nakagaki/src/"
           :base-extension "css\\|js\\|png\\|jpg\\|gif\\|pdf\\|mp3\\|ogg\\|swf"
           :publishing-directory "~/gene-nakagaki/dist/"
           :recursive t
           :publishing-function org-publish-attachment)
          ("gene" :components ("gene-org" "gene-static"))))
  )

(defun org-summary-todo (n-done n-not-done)
  "Switch entry to DONE when all subentries are done, to TODO otherwise."
  (let (org-log-done org-log-states)  ;turn off loggin
    (org-todo (if (= n-not-done 0) "DONE" "STARTED"))))

(add-hook! 'org-after-todo-statistics-hook 'org-summary-todo)
